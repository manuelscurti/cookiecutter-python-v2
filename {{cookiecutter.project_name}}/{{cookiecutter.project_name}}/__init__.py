"""**{{cookiecutter.project_name}}**

{{cookiecutter.description}}
"""
__version__ = "{{cookiecutter.version}}"

if __name__ == '__main__':
  print('Welcome to {{cookiecutter.project_name}}')
